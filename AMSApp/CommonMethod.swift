//
//  CommonMethod.swift
//  AMSApp
//
//  Created by Bill Chung on 9/4/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import Foundation

func DateTimeToDateTimeString(value : NSDate) -> String{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return formatter.string(from: (value) as Date)
}


func DateTimeToDateString(value : NSDate) -> String{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    return formatter.string(from: (value) as Date)
}


func CurrentYear() -> String{
    let date = Date()
    let calendar = Calendar.current
    let year = calendar.component(.year, from: date)
    
    return String(year) + "-01-01"
}


func NextYear() -> String{
    let date = Date()
    let calendar = Calendar.current
    let year = calendar.component(.year, from: date)
    
    return String(year+1) + "-01-01"
}

func IS08601stringToNSDate(value : String) -> NSDate{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    return dateformatter.date(from: value) as! NSDate
}
