//
//  UserProfile+CoreDataProperties.swift
//  AMSApp
//
//  Created by Bill Chung on 11/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//
//

import Foundation
import CoreData


extension UserProfile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProfile> {
        return NSFetchRequest<UserProfile>(entityName: "UserProfile")
    }

    @NSManaged public var code: String?
    @NSManaged public var value: String?

}
