//
//  Hotspot+CoreDataProperties.swift
//  AMSApp
//
//  Created by Bill Chung on 22/5/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//
//

import Foundation
import CoreData


extension Hotspot {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Hotspot> {
        return NSFetchRequest<Hotspot>(entityName: "Hotspot")
    }

    @NSManaged public var locationCode: String?
    @NSManaged public var locationDescription: String?
    @NSManaged public var iBeaconLocationMajor: String?
    @NSManaged public var iBeaconLocationMinor: String?

}
