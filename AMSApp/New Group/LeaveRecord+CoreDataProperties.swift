//
//  LeaveRecord+CoreDataProperties.swift
//  AMSApp
//
//  Created by Bill Chung on 4/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//
//

import Foundation
import CoreData


extension LeaveRecord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LeaveRecord> {
        return NSFetchRequest<LeaveRecord>(entityName: "LeaveRecord")
    }

    @NSManaged public var leaveStartDate: NSDate?
    @NSManaged public var leaveEndDate: NSDate?

}
