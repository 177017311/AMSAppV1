//
//  AttendanceRecord+CoreDataProperties.swift
//  AMSApp
//
//  Created by cmhin on 12/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//
//

import Foundation
import CoreData


extension AttendanceRecord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AttendanceRecord> {
        return NSFetchRequest<AttendanceRecord>(entityName: "AttendanceRecord")
    }

    @NSManaged public var location: String?
    @NSManaged public var recordTime: NSDate?
    @NSManaged public var status: String?
    @NSManaged public var major: String?
    @NSManaged public var minor: String?
    @NSManaged public var id: Int32

}
