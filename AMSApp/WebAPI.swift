//
//  WebAPI.swift
//  AMSApp
//
//  Created by Bill Chung on 4/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import Foundation

class WebAPI {
    static func getAPI(urlString: String){
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? String())
                return
            }
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary {
                OperationQueue.main.addOperation({
                    //calling another function after fetching the json
                    
                })
            }
            
        }).resume()
        
    }
}
