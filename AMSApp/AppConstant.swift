//
//  AppConstant.swift
//  AMSApp
//
//  Created by Bill Chung on 7/4/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import Foundation

struct ReturnStatus{
    static let success = "Success";
    static let fail =  "Fail";
}

struct ReturnJSONConstant{
    static let status = "status";
    static let message =  "message";
}

struct LeaveType{
    static var Data = ["Annual Leave","Sick Leave","Other Leave"]
}

struct AttendanceStatus {
    static let notSync = "Not Sync";
    static let created = "Created";
    static let accepted = "Accepted";
    static let rejected = "Rejected";
}

struct AbsenceStatus{
    static let created = "Created";
    static let approved = "Approved";
    static let rejected = "Rejected";
}
