//
//  ObjectClass.swift
//  AMSApp
//
//  Created by Bill Chung on 5/4/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit

class AttendanceRecordObject {
    var location: String!
    var recordTime: NSDate!
    var status: String!
}

class AbsenceRecordObject {
    var id: String!
    var leaveType: String!
    var leaveStartDate: NSDate!
    var leaveEndDate: NSDate!
    var remark: String!
    var approveStatus: String!
    var mangerRemark: String!
    var supportingImage: String!
}
