//
//  AppSetting.swift
//  AMSApp
//
//  Created by Bill Chung on 4/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import Foundation

struct AppSetting {
    static func GetWebServerPath() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            return dic["ServerPath"] as! String
        }
        return ""
    }
    
    static func GetUUID() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            return dic["UUID"] as! String
        }
        return ""
    }
    
    static func GetListAbsenceAPI() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            let serverPath = dic["ServerPath"] as! String
            let api = dic["ListAbsenceAPI"] as! String
            return serverPath + api
        }
        return ""
    }
    
    static func GetLoginAPI() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            let serverPath = dic["ServerPath"] as! String
            let api = dic["LoginAPI"] as! String
            return serverPath + api
        }
        return ""
    }
    
    static func GetChangePasswordAPI() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            let serverPath = dic["ServerPath"] as! String
            let api = dic["ChangePasswordAPI"] as! String
            return serverPath + api
        }
        return ""
    }
    
    static func GetCreateAbsenceAPI() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            let serverPath = dic["ServerPath"] as! String
            let api = dic["CreateAbsenceAPI"] as! String
            return serverPath + api
        }
        return ""
    }
    
    static func SubmitAttendance() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            let serverPath = dic["ServerPath"] as! String
            let api = dic["SubmitAttendance"] as! String
            return serverPath + api
        }
        return ""
    }
    
    static func GetAttendance() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            let serverPath = dic["ServerPath"] as! String
            let api = dic["ListAttendance"] as! String
            return serverPath + api
        }
        return ""
    }
    
    static func GetHotspot() -> String{
        let path = Bundle.main.path(forResource: "AppSetting", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: path!) as? [String: Any] {
            let serverPath = dic["ServerPath"] as! String
            let api = dic["ListHotspot"] as! String
            return serverPath + api
        }
        return ""
    }
    
}


