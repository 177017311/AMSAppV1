//
//  LoginViewController.swift
//  AMSApp
//
//  Created by Bill Chung on 10/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit
import CoreData

class LoginViewController: UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    @IBOutlet weak var AccountField: UITextField!
    @IBOutlet weak var PasswordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        managedObjectContext = appDelegate.persistentContainer.viewContext
        print((NSPersistentContainer.defaultDirectoryURL()))
        
        if (self.GetUserName() != ""){
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "ShowMenu", sender: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func Login(_ sender: Any) {

            if(AccountField.text! == ""){
                let alert = UIAlertController(title: "Error", message: "Please input your account", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return;
            }
            
            if(PasswordField.text! == ""){
                let alert = UIAlertController(title: "Error", message: "Please input your password", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return;
            }
        
            let json: [String: Any] = ["Email": AccountField.text!,
                                       "Password":PasswordField.text!,
                                       "RememberMe": "false"]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
            // create post request
            let url = URL(string: AppSetting.GetLoginAPI())!
            print(url)
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            // insert json data to the request
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print(error?.localizedDescription ?? "No data")
                    return
                }
                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)

                    if (responseJSON[ReturnJSONConstant.status]! as! String == ReturnStatus.success){
                        self.insertUserDate(Code: "StaffID", Value: responseJSON["Id"] as! String)
                        self.insertUserDate(Code: "StaffEmail", Value: responseJSON["email"] as! String)
//                        self.insertUserDate(Code: "StaffPhoneNo", Value: responseJSON["phoneNo"] as! String)
                        self.insertUserDate(Code: "StaffTitle", Value: responseJSON["staffTitle"] as! String)
                        self.insertUserDate(Code: "StaffName", Value: responseJSON["staffName"] as! String)
                        
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "MainViewController")
                            self.present(controller, animated: true, completion: nil)
                        }
                        
                    }else{
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Error", message: responseJSON[ReturnJSONConstant.message] as! String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            return;
                        }

                    }
                }
            }
            
            task.resume()
        
    }
    
    func insertUserDate(Code :String, Value : String){
        
        do {
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
            let allUserProfile = try managedObjectContext.fetch(fetchRequest)
            for userProfile in allUserProfile as! [UserProfile]{
                if (userProfile.code! == Code){
                    userProfile.value = Value
                    appDelegate.saveContext()
                    return;
                }
            }
            
            
            let userProfile = NSEntityDescription.insertNewObject(forEntityName: "UserProfile",into: self.managedObjectContext!) as! UserProfile
            userProfile.code = Code
            userProfile.value = Value
            appDelegate.saveContext()
            
        }catch{
            print(error)
        }
        
    }
    
    func GetUserName() -> String{
        do {
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
            let allUserProfile = try managedObjectContext.fetch(fetchRequest)
            for userProfile in allUserProfile as! [UserProfile]{
                if (userProfile.code! == "StaffID"){
                    return userProfile.value!
                }
            }
        }catch{
            print(error)
        }
        return""
    }

    
}
