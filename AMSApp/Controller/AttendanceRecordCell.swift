//
//  AttendanceRecordCell.swift
//  AMSApp
//
//  Created by Bill Chung on 4/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit

class AttendanceRecordCell: UITableViewCell {
    
    @IBOutlet var RecordTime: UILabel?
    @IBOutlet var Location: UILabel?
    @IBOutlet var Status: UILabel?
    @IBOutlet var icon: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

