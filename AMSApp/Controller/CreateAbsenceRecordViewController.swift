//
//  CreateAbsenceRecordViewController.swift
//  AMSApp
//
//  Created by Bill Chung on 22/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit
import CoreData

class CreateAbsenceRecordViewController: UITableViewController ,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var leaveType: UITextField!
    @IBOutlet weak var StartDate: UITextField!
    @IBOutlet weak var EndDate: UITextField!
    @IBOutlet weak var Remark: UITextField!
    
    let imagePickerVC = UIImagePickerController()
    var LeaveTypeData = LeaveType.Data
    var LeaveTypePicker = UIPickerView()
    var base64image = ""
    
    @IBAction func AddSupporting(_ sender: Any) {
        imagePickerVC.delegate = self
        imagePickerVC.sourceType = .photoLibrary
        imagePickerVC.modalPresentationStyle = .popover
//        let popover = imagePickerVC.popoverPresentationController
//        popover?.sourceView = sender as! UIView
//
//        popover?.sourceRect = (sender as AnyObject).bounds
//        popover?.permittedArrowDirections = .any
        
        show(imagePickerVC,sender: self)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageView.image = image
       imageView.contentMode = UIViewContentMode.scaleAspectFit
        
        let imageData:NSData = UIImagePNGRepresentation(imageView.image!)! as NSData
        let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        base64image = imageStr
        
        dismiss(animated: true, completion: nil)
    }
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    override func viewDidLoad() {
        super.viewDidLoad()
        managedObjectContext = appDelegate.persistentContainer.viewContext
        imagePickerVC.delegate = self
        
        LeaveTypePicker.delegate = self
        LeaveTypePicker.dataSource = self
        leaveType.inputView = LeaveTypePicker
        
        let StartDatePicker = UIDatePicker()
        StartDatePicker.datePickerMode = .date
        StartDatePicker.date = NSDate() as Date
        
        let EndDatePicker = UIDatePicker()
        EndDatePicker.datePickerMode = .date
        EndDatePicker.date = NSDate() as Date
        
        StartDatePicker.addTarget(
            self,
            action: #selector(self.StartDatePickerChanged),
            for: .valueChanged)
        StartDate.inputView = StartDatePicker
        StartDate.tag = 200
        
        EndDatePicker.addTarget(
            self,
            action: #selector(self.EndDatePickerChanged),
            for: .valueChanged)
        EndDate.inputView = EndDatePicker
        EndDate.tag = 201
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc func StartDatePickerChanged(datePicker:UIDatePicker) {
        let myTextField = self.view?.viewWithTag(200) as? UITextField
        myTextField?.text = DateTimeToDateString(value: datePicker.date as NSDate)
    }
    
    @objc func EndDatePickerChanged(datePicker:UIDatePicker) {
        let myTextField = self.view?.viewWithTag(201) as? UITextField
        myTextField?.text = DateTimeToDateString(value: datePicker.date as NSDate)
    }
    
    @objc func closeKeyboard(){
            self.view.endEditing(true)
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
            return LeaveTypeData.count
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           leaveType.text = LeaveTypeData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return LeaveTypeData[row]
        return ""
    }
    
    @IBAction func Cancel(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Create(_ sender: Any) {
        
        
        let json: [String: Any] = ["Id" : GetUserName(),
                                   "leaveType" : leaveType.text,
                                   "leaveStartDate" : StartDate.text,
                                   "leaveEndDate" : EndDate.text,
                                   "remark" : Remark.text,
                                   "image" : base64image]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: AppSetting.GetCreateAbsenceAPI())!
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
                
                if (responseJSON[ReturnJSONConstant.status]! as! String == ReturnStatus.success){
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Record Changed", message: responseJSON[ReturnJSONConstant.message] as! String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Error", message: responseJSON[ReturnJSONConstant.message] as! String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return;
                    }
                    
                }
            }
        }
        
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    
    func GetUserName() -> String{
        do {
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
            let allUserProfile = try managedObjectContext.fetch(fetchRequest)
            for userProfile in allUserProfile as! [UserProfile]{
                if (userProfile.code! == "StaffID"){
                    return userProfile.value!
                }
            }
        }catch{
            print(error)
        }
        return""
    }
}
