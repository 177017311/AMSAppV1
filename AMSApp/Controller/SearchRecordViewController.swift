//
//  SearchRecordViewController.swift
//  AMSApp
//
//  Created by cmhin on 19/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit
import CoreData

class SearchRecordViewController: UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    var attendanceRecords = [AttendanceRecordObject]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        managedObjectContext = appDelegate.persistentContainer.viewContext
        
        queryUserData()
    }
    
    func queryUserData(){
        do {
            attendanceRecords = [AttendanceRecordObject]()
            
            
            let json: [String: Any] = ["Id": GetUserName(),
                                       "StartDate": CurrentYear(),
                                       "EndDate": NextYear()
            ]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
            // create post request
            let url = URL(string: AppSetting.GetAttendance())!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            // insert json data to the request
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print(error?.localizedDescription ?? "No data")
                    return
                }
                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                if let responseJSON = responseJSON as? [String: Any] {
                    print(responseJSON)
                    
                    if (responseJSON[ReturnJSONConstant.status]! as! String == ReturnStatus.success){
                        let jsonArray = responseJSON["attendanceRecordList"] as? [[String: Any]]
                        
                        for json in jsonArray! {
                            let item = json as? [String: Any]
                            
                            let attendanceRecord = AttendanceRecordObject()
                            attendanceRecord.location = item!["location"] as? String
                            
                            attendanceRecord.recordTime = IS08601stringToNSDate(value: (item!["attendanceTimeStamp"] as? String)!)
                            attendanceRecord.status = item!["status"] as? String
                            self.attendanceRecords.append(attendanceRecord)
                        }
                    }else{
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Error", message: responseJSON[ReturnJSONConstant.message] as! String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            return;
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
            task.resume()
            
        }catch{
            print(error)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.attendanceRecords.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceRecordCell", for: indexPath)
        
        if let attendanceRecordCell = cell as? AttendanceRecordCell {
            let attendanceRecord = self.attendanceRecords[indexPath.row]
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm"
            let myString = formatter.string(from: (attendanceRecord.recordTime as! NSDate) as Date)
            
            attendanceRecordCell.RecordTime?.text = myString
            attendanceRecordCell.Location?.text = attendanceRecord.location
            attendanceRecordCell.Status?.text = attendanceRecord.status
        }
        
        return cell
    }
    
    func GetUserName() -> String{
        do {
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
            let allUserProfile = try managedObjectContext.fetch(fetchRequest)
            for userProfile in allUserProfile as! [UserProfile]{
                if (userProfile.code! == "StaffID"){
                    return userProfile.value!
                }
            }
        }catch{
            print(error)
        }
        return""
    }
    
    @IBAction func unwindSegueBack(segue: UIStoryboardSegue){
        let source = segue.source as? SearchRecordCriteriaViewController

            self.tableView.reloadData()
    
    }
    
}
