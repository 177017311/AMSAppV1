    //
//  HomeViewController.swift
//  AMSApp
//
//  Created by Bill Chung on 4/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit
import CoreData
import CoreBluetooth
import CoreLocation
import AudioToolbox
import UserNotifications
    
class HomeViewController: UITableViewController,CLLocationManagerDelegate,UNUserNotificationCenterDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    var attendanceRecords = [AttendanceRecord]()
    var currentLocationMarked = false;
    let iBeaconLM = CLLocationManager()
    let center = UNUserNotificationCenter.current()

    override func viewDidLoad() {
        super.viewDidLoad()
        center.delegate = self
        
        iBeaconLM.requestAlwaysAuthorization()
        iBeaconLM.delegate = self
        
        let uuid = UUID(uuidString: AppSetting.GetUUID())
        let region = CLBeaconRegion(proximityUUID: uuid!, identifier: "myregion")
        
        iBeaconLM.startRangingBeacons(in: region)
        iBeaconLM.startMonitoring(for: region)
        
        managedObjectContext = appDelegate.persistentContainer.viewContext
        print((NSPersistentContainer.defaultDirectoryURL()))
        
        queryUserData()
        updateHotspot()
    }
    
    func submitAttendanceReocrd(){
        do{
            
            let userName = self.GetUserName()
            attendanceRecords = [AttendanceRecord]()
            let fetchRequest = NSFetchRequest<AttendanceRecord>(entityName: "AttendanceRecord")
            fetchRequest.predicate = NSPredicate(format: "status == %@", AttendanceStatus.notSync)
            let allAttendanceRecord = try managedObjectContext.fetch(fetchRequest)
            for attendanceRecord in allAttendanceRecord {
                let json: [String: Any] = ["attendanceID" : String(describing: attendanceRecord.id),
                                           "Id": userName,
                                           "attendanceTimeStamp": DateTimeToDateTimeString(value: attendanceRecord.recordTime!),
                                           "iBeaconLocationMajor": attendanceRecord.major as! String,
                                           "iBeaconLocationMinor": attendanceRecord.minor as! String,
                ]
                
                
                print(String(describing: attendanceRecord.recordTime))
                let jsonData = try? JSONSerialization.data(withJSONObject: json)
                
                let url = URL(string: AppSetting.SubmitAttendance())!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonData
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                    if let responseJSON = responseJSON as? [String: Any] {
                        if (responseJSON[ReturnJSONConstant.status]! as! String == ReturnStatus.success){
                            self.AccreptAttendanceRecord(AttendanceID : responseJSON["attendanceID"] as! String)
                        }
                    }
                }
                
                task.resume()
            }
        }catch{
            print(error)
        }
    }
    
    func updateHotspot(){
        do{
            
                let url = URL(string: AppSetting.GetHotspot())!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            
                let json: [String: Any] = [:]
                let jsonData = try? JSONSerialization.data(withJSONObject: json)
                request.httpBody = jsonData
            
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                    if let responseJSON = responseJSON as? [String: Any] {
                        if (responseJSON[ReturnJSONConstant.status]! as! String == ReturnStatus.success){
                            let jsonArray = responseJSON["hotspotList"] as? [[String: Any]]
                            
                            for json in jsonArray! {
                                let item = json as? [String: Any]
                                self.insertLocationDate(LocationCode: (item!["LocationCode"] as? String)!, LocationDescription: (item!["LocationDescription"] as? String)! ,iBeaconLocationMajor: (item!["iBeaconLocationMajor"] as? String)!,iBeaconLocationMinor: (item!["iBeaconLocationMinor"] as? String)!)
                            }
                        }
                    }
                    
                }
                
                task.resume()
            
        }catch{
            print(error)
        }
    }
    
    func insertLocationDate(LocationCode :String,LocationDescription :String,iBeaconLocationMajor :String,iBeaconLocationMinor :String){
        
        do {
            let fetchRequest = NSFetchRequest<Hotspot>(entityName: "Hotspot")
            let allHotspot = try managedObjectContext.fetch(fetchRequest)
            for hotspot in allHotspot as! [Hotspot]{
                if (hotspot.locationCode! == LocationCode){
                    hotspot.locationDescription = LocationDescription
                    hotspot.iBeaconLocationMajor = iBeaconLocationMajor
                    hotspot.iBeaconLocationMinor = iBeaconLocationMinor
                    appDelegate.saveContext()
                    return;
                }
            }
            
            
            let hotspot = NSEntityDescription.insertNewObject(forEntityName: "Hotspot",into: self.managedObjectContext!) as! Hotspot
            hotspot.locationCode = LocationCode
            hotspot.locationDescription = LocationDescription
            hotspot.iBeaconLocationMajor = iBeaconLocationMajor
            hotspot.iBeaconLocationMinor = iBeaconLocationMinor
            appDelegate.saveContext()
            
        }catch{
            print(error)
        }
        
    }
    
    func getLocationDate(Major :String,Minor :String) -> String{
        
        do {
            let fetchRequest = NSFetchRequest<Hotspot>(entityName: "Hotspot")
            let allHotspot = try managedObjectContext.fetch(fetchRequest)
            for hotspot in allHotspot as! [Hotspot]{
                if (hotspot.iBeaconLocationMajor! == Major){
                    if (hotspot.iBeaconLocationMinor! == Minor){
                                  return hotspot.locationDescription!;
                    }
                }
            }
        }catch{
            print(error)
        }
        
         return "Unknown Location"
        
    }
    
    func insertAttendanceRecord(major :String, minor :String){
        
        let attendanceRecord = NSEntityDescription.insertNewObject(forEntityName: "AttendanceRecord",into: self.managedObjectContext!) as! AttendanceRecord
        
        let myUserDefaults = UserDefaults.standard
        var seq = 1
        if let idSeq = myUserDefaults.object(forKey: "idSeq")
            as? Int {
            seq = idSeq + 1
        }
        attendanceRecord.id = Int32(seq);
        attendanceRecord.recordTime = NSDate()
        attendanceRecord.location = getLocationDate(Major: major, Minor: minor)
        attendanceRecord.major = major
        attendanceRecord.minor = minor
        attendanceRecord.status = "Not Sync"
        appDelegate.saveContext()
        
        myUserDefaults.set(seq, forKey: "idSeq")
        
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert,.sound,.badge]){
            (granted, error) in
            if granted == false {
                print("Not allow to notidication")
            }
        }
        
        //        center.setNotificationCategories()
        sendNotification(location: attendanceRecord.location!, recordDatetime: attendanceRecord.recordTime!)
        
    }
    
    func AccreptAttendanceRecord(AttendanceID :String){
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "AttendanceRecord")
        let predicate = NSPredicate(format: "id = '\(AttendanceID)'")
        fetchRequest.predicate = predicate
        do
        {
            let AttendanceRecord = try managedObjectContext.fetch(fetchRequest)

            let objectUpdate = AttendanceRecord[0] as! NSManagedObject
            objectUpdate.setValue(AttendanceStatus.accepted, forKey: "status")
            do{
                try managedObjectContext.save()
            }
            catch
            {
                print(error)
            }
        }
        catch
        {
            print(error)
        }
    }
    
    func queryUserData(){
        do {
            attendanceRecords = [AttendanceRecord]()
            let fetchRequest = NSFetchRequest<AttendanceRecord>(entityName: "AttendanceRecord")
            let sort = NSSortDescriptor(key: #keyPath(AttendanceRecord.recordTime), ascending: false)
            fetchRequest.sortDescriptors = [sort]
            fetchRequest.fetchLimit = 10
            let allAttendanceRecord = try managedObjectContext.fetch(fetchRequest)
            for attendanceRecord in allAttendanceRecord {
                attendanceRecords.append(attendanceRecord)
            }
        }catch{
            print(error)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons:[CLBeacon],  in region: CLBeaconRegion) {
        for beacon in beacons{
            let logTime = DateTimeToDateString(value: Date() as NSDate)
            
            print("major=\(beacon.major) minor=\(beacon.minor) accury=\(beacon.accuracy) rssi=\(beacon.rssi)")
            switch beacon.proximity{
            case .far:
                if (!currentLocationMarked){
                    print(logTime + " beacon far")
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    insertAttendanceRecord(major: String(describing: beacon.major),minor: String(describing:beacon.minor))
                    submitAttendanceReocrd()
                    queryUserData()
                    self.tableView.reloadData()
                    currentLocationMarked = true
                }
            case .near:
                if (!currentLocationMarked){
                    print(logTime + " beacon near")
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    insertAttendanceRecord(major: String(describing: beacon.major),minor: String(describing:beacon.minor))
                    submitAttendanceReocrd()
                    queryUserData()
                    self.tableView.reloadData()
                    currentLocationMarked = true
                }
            case .immediate:
                if (!currentLocationMarked){
                    print(logTime + " beacon immediate")
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    insertAttendanceRecord(major: String(describing: beacon.major),minor: String(describing:beacon.minor))
                    submitAttendanceReocrd()
                    queryUserData()
                    self.tableView.reloadData()
                    currentLocationMarked = true
                    
                }
            case .unknown:
                print(logTime + " beacon unknown")
                queryUserData()
                self.tableView.reloadData()
                currentLocationMarked = false
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager,didEnterRegion region: CLRegion) {
        print("Enter \(region.identifier)")
        self.tableView.reloadData()
    }
    
    func locationManager(_ manager: CLLocationManager,didExitRegion region: CLRegion) {
        print("Exit \(region.identifier)")
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.attendanceRecords.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceRecordCell", for: indexPath)
        
        if let attendanceRecordCell = cell as? AttendanceRecordCell {
            let attendanceRecord = self.attendanceRecords[indexPath.row]
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myString = formatter.string(from: (attendanceRecord.recordTime as! NSDate) as Date)
            
            attendanceRecordCell.RecordTime?.text = myString
            attendanceRecordCell.Location?.text = attendanceRecord.location
            attendanceRecordCell.Status?.text = attendanceRecord.status
        }
        
        return cell
    }
    
    func GetUserName() -> String{
        do {
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
            let allUserProfile = try managedObjectContext.fetch(fetchRequest)
            for userProfile in allUserProfile as! [UserProfile]{
                if (userProfile.code! == "StaffID"){
                    return userProfile.value!
                }
            }
        }catch{
            print(error)
        }
        return""
    }
    

    
    func sendNotification(location : String, recordDatetime: NSDate){
        let content = UNMutableNotificationContent()
        content.title = "Take attendance"
        content.body = "You are enterd the location : " + location + " at " + recordDatetime.addingTimeInterval(8).description
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest(identifier: "myid", content: content, trigger: trigger)
        
        center.add(request)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
         completionHandler([.alert])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}
