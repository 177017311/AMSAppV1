//
//  AbsenceRecordViewController.swift
//  AMSApp
//
//  Created by Bill Chung on 22/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit
import CoreData

class AbsenceRecordViewController: UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    var absenceRecords = [AbsenceRecordObject]()
    var selectedRow = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        managedObjectContext = appDelegate.persistentContainer.viewContext
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        queryUserData()
    }
    
    func queryUserData(){
        do {
            absenceRecords = [AbsenceRecordObject]()
            
            
            let json: [String: Any] = ["Id": GetUserName(),
                                       "StartDate": CurrentYear(),
                                       "EndDate": NextYear()
            ]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
            // create post request
            let url = URL(string: AppSetting.GetListAbsenceAPI())!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            // insert json data to the request
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print(error?.localizedDescription ?? "No data")
                    return
                }
                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                if let responseJSON = responseJSON as? [String: Any] {
                    print(responseJSON)
                    
                    if (responseJSON[ReturnJSONConstant.status]! as! String == ReturnStatus.success){
                        let jsonArray = responseJSON["absenceRecordList"] as? [[String: Any]]
                        
                        for json in jsonArray! {
                            let item = json as? [String: Any]
                            
                            let absenceRecord = AbsenceRecordObject()
                            absenceRecord.id = item!["absenceID"] as? String
                            absenceRecord.leaveStartDate = IS08601stringToNSDate(value: (item!["leaveStartDate"] as? String)!)
                            absenceRecord.leaveEndDate = IS08601stringToNSDate(value: (item!["leaveEndDate"] as? String)!)
                            absenceRecord.leaveType = item!["leaveType"] as? String
                            absenceRecord.remark = item!["remark"] as? String
                            absenceRecord.approveStatus = item!["approveStatus"] as? String
                            absenceRecord.mangerRemark = item!["managerRemark"] as? String
                            absenceRecord.supportingImage = item!["imagePath"] as? String
                            self.absenceRecords.append(absenceRecord)
                        }
                    }else{
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Error", message: responseJSON[ReturnJSONConstant.message] as! String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            return;
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
            task.resume()
            
        }catch{
            print(error)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewAbsenceDetail" {
            let DestViewController = segue.destination as! UINavigationController
            let targetController = DestViewController.topViewController as! ViewAbsenceRecordViewController
            let indexPath = tableView.indexPathForSelectedRow
            targetController.absenceRecordObject = self.absenceRecords[(indexPath?.row)!]
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.absenceRecords.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AbsenceRecordCell", for: indexPath)
        
        if let absenceRecordCell = cell as? AbsenceRecordCell {
            let absenceRecord = self.absenceRecords[indexPath.row]
            absenceRecordCell.StartDate?.text = DateTimeToDateString(value: absenceRecord.leaveStartDate as! NSDate) + " - " + DateTimeToDateString(value: absenceRecord.leaveEndDate as! NSDate)
            absenceRecordCell.LeaveType?.text = absenceRecord.leaveType
            absenceRecordCell.Status?.text = absenceRecord.approveStatus
        }
        
        return cell
    }
    
    func GetUserName() -> String{
        do {
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
            let allUserProfile = try managedObjectContext.fetch(fetchRequest)
            for userProfile in allUserProfile as! [UserProfile]{
                if (userProfile.code! == "StaffID"){
                    return userProfile.value!
                }
            }
        }catch{
            print(error)
        }
        return""
    }
}


