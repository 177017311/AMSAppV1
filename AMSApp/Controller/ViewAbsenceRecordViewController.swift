//
//  ViewAbsenceRecordViewController.swift
//  AMSApp
//
//  Created by Bill Chung on 22/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit
import CoreData

class ViewAbsenceRecordViewController: UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    var absenceRecordObject : AbsenceRecordObject!
    
    @IBOutlet weak var LeaveType: UITextField!
    @IBOutlet weak var StartDate: UITextField!
    @IBOutlet weak var EndDate: UITextField!
    @IBOutlet weak var Remark: UITextField!
    @IBOutlet weak var Status: UITextField!
    @IBOutlet weak var ManagerRemark: UITextField!
        @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        managedObjectContext = appDelegate.persistentContainer.viewContext

        LeaveType.text = absenceRecordObject.leaveType
        StartDate.text = DateTimeToDateString(value: absenceRecordObject.leaveStartDate!)
        EndDate.text = DateTimeToDateString(value: absenceRecordObject.leaveEndDate!)
        Remark.text = absenceRecordObject.remark
        Status.text = absenceRecordObject.approveStatus
        ManagerRemark.text = absenceRecordObject.mangerRemark
        Status.text = absenceRecordObject.approveStatus
        
        if (!absenceRecordObject.supportingImage.isEmpty){
            let url = URL(string: absenceRecordObject.supportingImage)
            let data = try? Data(contentsOf: url!)
            imageView.contentMode = UIViewContentMode.scaleAspectFit
            imageView.image = UIImage(data: data!)
        }

    }
    
    
    @IBAction func Cancel(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetUserName() -> String{
        do {
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
            let allUserProfile = try managedObjectContext.fetch(fetchRequest)
            for userProfile in allUserProfile as! [UserProfile]{
                if (userProfile.code! == "StaffID"){
                    return userProfile.value!
                }
            }
        }catch{
            print(error)
        }
        return ""
    }
}
