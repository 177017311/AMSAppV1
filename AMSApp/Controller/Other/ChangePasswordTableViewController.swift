//
//  ChangePasswordTableViewController.swift
//  AMSApp
//
//  Created by Bill Chung on 4/3/2018.
//  Copyright © 2018年 Bill Chung. All rights reserved.
//

import UIKit
import CoreData

class ChangePasswordTableViewController: UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var managedObjectContext : NSManagedObjectContext!
    
    
    @IBOutlet weak var OldPassword: UITextField!
    @IBOutlet weak var NewPassword: UITextField!
    @IBOutlet weak var ConfirmNewPassword: UITextField!
    
    @IBAction func Cancel(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        managedObjectContext = appDelegate.persistentContainer.viewContext

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Save(_ sender: Any) {
        
        if(OldPassword.text! == ""){
            let alert = UIAlertController(title: "Error", message: "Please input your old password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if(NewPassword.text! == ""){
            let alert = UIAlertController(title: "Error", message: "Please input your new password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if(ConfirmNewPassword.text! == ""){
            let alert = UIAlertController(title: "Error", message: "Please input your confirm new password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        if(ConfirmNewPassword.text! != NewPassword.text!){
            let alert = UIAlertController(title: "Error", message: "Confirm new password not match", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        let json: [String: Any] = ["Id": GetUserName(),
                                   "OldPassword":OldPassword.text!,
                                    "NewPassword":NewPassword.text!,
                                    "ConfirmPassword":ConfirmNewPassword.text!]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        let url = URL(string: AppSetting.GetChangePasswordAPI())!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
                
                if (responseJSON[ReturnJSONConstant.status]! as! String == ReturnStatus.success){
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Password Changed", message: responseJSON[ReturnJSONConstant.message] as! String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }else{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Error", message: responseJSON[ReturnJSONConstant.message] as! String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return;
                    }
                    
                }
            }
        }
        
        task.resume()
        
    }
    
    func GetUserName() -> String{
        do {
            let fetchRequest = NSFetchRequest<UserProfile>(entityName: "UserProfile")
            let allUserProfile = try managedObjectContext.fetch(fetchRequest)
            for userProfile in allUserProfile as! [UserProfile]{
                if (userProfile.code! == "StaffID"){
                    return userProfile.value!
                }
            }
        }catch{
            print(error)
        }
        return""
    }
}

